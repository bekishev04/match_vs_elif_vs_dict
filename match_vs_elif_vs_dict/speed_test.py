import time

from match_vs_elif_vs_dict.enums import BlockType
from match_vs_elif_vs_dict.generated import dict1_, dict2_, if_, elif_, match_


def test(value: int, repeat: int, test_type: BlockType = None):

    if test_type:
        blocks = [test_type]
    else:
        blocks = list(BlockType)

    for type_ in blocks:
        t0 = time.time()

        for _ in range(repeat):
            eval(f"{type_.module_name}.run({value})")

        t1 = time.time()

        print(type_.module_name, t1 - t0)
