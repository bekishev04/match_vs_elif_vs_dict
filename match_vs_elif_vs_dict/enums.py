from __future__ import annotations

import enum
from match_vs_elif_vs_dict import generator


class BlockType(enum.Enum):
    IF_ = generator.BlockIf
    ELIF_ = generator.BlockElif
    MATCH_ = generator.BlockMatch
    DICT1_ = generator.BlockDictInFunc
    DICT2_ = generator.BlockDictOutFunc

    def __new__(cls, value: generator.AbstractBlock) -> generator.AbstractBlock:
        return value
