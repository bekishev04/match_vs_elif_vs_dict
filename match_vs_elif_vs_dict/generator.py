from __future__ import annotations

from abc import abstractmethod
from os import path


class MetaBlock(type):
    value: str

    def __repr__(cls) -> str:
        return cls.value


class AbstractBlock(metaclass=MetaBlock):
    ident: int = 4
    tabs: str = " " * ident
    folder: str = path.join(path.dirname(path.abspath(__file__)), "generated")
    varname: str = "value"
    pattern: str = f"def run({varname}: int):\n{{}}"

    @classmethod
    @property
    def module_name(cls) -> str:
        """Return name of module"""

        return cls.value + "_"

    @classmethod
    @property
    def file_name(cls) -> str:
        """Return name of file"""
        return cls.module_name + ".py"

    @abstractmethod
    def generate_code(cls, max_value: int) -> str:
        """Generate code of block"""
        raise NotImplementedError

    @classmethod
    def add_tabs_in_code(cls, code_: str) -> str:
        """Add tabs in code for paste in function or method"""
        # return code_
        return "\n".join(map(lambda x: cls.tabs + x if x else x, code_.split("\n")))

    @classmethod
    def generate_file(cls, max_value: int) -> None:
        """Method for generate file"""
        file = open(path.join(cls.folder, cls.file_name), "w")

        code_ = cls.generate_code(max_value)
        code_ = cls.add_tabs_in_code(code_)
        code_ = cls.pattern.format(code_)

        file.write(code_)
        file.close()


class BlockIf(AbstractBlock):
    value = "if"

    @classmethod
    def generate_code(cls, max_value: int) -> str:
        """Method for generate code with if blocks"""

        code_ = ""
        for i in range(max_value):
            code_ += f"if {cls.varname} == {i}:\n{cls.tabs}return {cls.varname} ** 2\n"
        return code_


class BlockElif(AbstractBlock):
    value = "elif"

    @classmethod
    def generate_code(cls, max_value: int) -> str:
        """Method for generate code with elif blocks"""

        code_ = f"if {cls.varname} == {0}:\n{cls.tabs}return {cls.varname} ** 2\n"
        for i in range(1, max_value - 1):
            code_ += (
                f"elif {cls.varname} == {i}:\n{cls.tabs}return {cls.varname} ** 2\n"
            )
        return code_


class BlockMatch(AbstractBlock):
    value = "match"

    @classmethod
    def generate_code(cls, max_value: int) -> str:
        """Method for generate code with match blocks"""

        code_ = f"match {cls.varname}:\n"
        for i in range(max_value):
            code_ += f"{cls.tabs}case {i}:\n{cls.tabs * 2}return {cls.varname} ** 2\n"
        return code_


class BlockDictInFunc(AbstractBlock):
    value = "dict1"

    @classmethod
    def generate_code(cls, max_value: int) -> str:
        """Method for generate dict"""

        code_ = f"{cls.value} = {{\n"
        for i in range(max_value):
            code_ += f"{cls.tabs}{i}: {i} ** 2,\n"
        code_ += f"}}\n\n"

        code_ += f"return {cls.value}.get({cls.varname}, None)\n"

        return code_


class BlockDictOutFunc(AbstractBlock):
    value = "dict2"

    @classmethod
    def generate_code(cls, max_value: int) -> str:
        """Method for generate dict"""

        code_ = f"{cls.value} = {{\n"
        for i in range(max_value):
            code_ += f"{cls.tabs}{i}: {i} ** 2,\n"
        code_ += f"}}\n\n"
        return code_

    @classmethod
    def generate_file(cls, max_value: int) -> None:
        """Method for generate file"""
        file = open(path.join(cls.folder, cls.file_name), "w")

        code_ = (
            cls.generate_code(max_value)
            + "\n"
            + cls.pattern.format(
                f"{cls.tabs}return {cls.value}.get({cls.varname}, None)\n"
            )
        )

        file.write(code_)
        file.close()
