from match_vs_elif_vs_dict import speed_test, enums
from match_vs_elif_vs_dict.enums import BlockType
import argparse


def generate():
    parser = argparse.ArgumentParser(description="Generate python file for speed test")

    parser.add_argument(
        "-max_value",
        type=int,
        default=300,
        help="argument -max_value use for end of blocks",
    )

    args = parser.parse_args()

    max_value = args.max_value + 1

    for type_ in list(BlockType):
        type_.generate_file(max_value)


def test():
    parser = argparse.ArgumentParser(description="Speed test")

    parser.add_argument(
        "-value",
        default=1,
        type=int,
        help="argument -value use in all of generated case",
    )
    parser.add_argument(
        "-repeat",
        type=int,
        default=1000,
        help="argument -repeat use for number of repeat",
    )
    parser.add_argument(
        "-type",
        type=enums.BlockType,
        help="argument -type use for test only this type. If this value default ",
    )

    args = parser.parse_args()

    value = args.value
    repeat = args.repeat
    type_ = args.type

    speed_test.test(value, repeat, test_type=type_)
